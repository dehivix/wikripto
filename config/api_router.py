from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from wikripto.coins.views import CoinsOperationListView
from wikripto.users.api.views import UserViewSet
from wikripto.coins.api.views import CoinsViewSet, CoinsOperationsViewSet

"""Define the router to the api urls"""
if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

"""Register urls to be used in REST-API"""
#router.register("users", UserViewSet)
router.register("coins", CoinsViewSet)
router.register("coins_operations", CoinsOperationsViewSet)

"""Finnaly inserting urls to the aplication router and getting ready for queries"""
app_name = "api"
urlpatterns = router.urls
