from django.contrib.auth import get_user_model
from rest_framework import serializers
from wikripto.coins.models import Coins, CoinsOperations

class CoinsSerializer(serializers.ModelSerializer):
    """DRF serializer class to represent/save the Coins objects"""
    class Meta:
        model = Coins
        fields = "__all__"

        extra_kwargs = {
            "url": {"view_name": "api:coins-detail"}
        }


class CoinsOperationsSerializer(serializers.ModelSerializer):
    """DRF serializer class to represent/save the Coins operations objects"""
    class Meta:
        model = CoinsOperations
        fields = "__all__"

        extra_kwargs = {
            "url": {"view_name": "api:coins-operations"}
        }
