
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import OrderingFilter
from wikripto.coins.models import Coins, CoinsOperations
from .serializers import CoinsOperationsSerializer, CoinsSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework import filters


class StandardResultsSetPagination(PageNumberPagination):
    """Override the paginaton class to define own parameters"""
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CoinsViewSet(ModelViewSet):
    """viewSet to define and operate with Coins objects based on models"""
    serializer_class = CoinsSerializer
    queryset = Coins.objects.all()
    pagination_class = StandardResultsSetPagination
    lookup_field = "pk"   
    search_fields = ['name', 'symbol']
    filter_backends = [
        filters.SearchFilter,
        OrderingFilter,
    ]
    
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        # make sure to catch 404's below
        obj = queryset.get(pk=self.request.user.pk)
        self.check_object_permissions(self.request, obj)
        return obj
        
    def get_queryset(self, *args, **kwargs):
        #assert isinstance(self.request.user.id, int)
        return self.queryset.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    @action(detail=False)
    def me(self, request):
        serializer = CoinsSerializer(request.user, context={"request": request})
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class CoinsOperationsViewSet(ModelViewSet):
    """viewSet to define and operate with Coins objects based on models"""
    serializer_class = CoinsOperationsSerializer
    pagination_class = StandardResultsSetPagination
    queryset = CoinsOperations.objects.all()
    lookup_field = "pk"
    
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        # make sure to catch 404's below
        obj = queryset.get(pk=self.request.user.pk)
        self.check_object_permissions(self.request, obj)
        return obj
        
    def get_queryset(self, *args, **kwargs):
        #assert isinstance(self.request.user.id, int)
        return self.queryset.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    @action(detail=False)
    def me(self, request):
        serializer = CoinsOperationsSerializer(request.user, context={"request": request})
        return Response(status=status.HTTP_200_OK, data=serializer.data)
    