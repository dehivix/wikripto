# Generated by Django 3.1.13 on 2021-11-20 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Coins',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('SNo', models.IntegerField()),
                ('name', models.CharField(max_length=30)),
                ('symbol', models.CharField(max_length=6)),
                ('date', models.DateField()),
                ('high', models.FloatField()),
                ('low', models.FloatField()),
                ('open_val', models.FloatField()),
                ('close_val', models.FloatField()),
                ('volume', models.FloatField()),
                ('marketcap', models.FloatField()),
            ],
            options={
                'verbose_name_plural': 'coins',
            },
        ),
    ]
