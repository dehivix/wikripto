from django.contrib import admin
from .models import Coins, CoinsOperations

class CoinsAdmin(admin.ModelAdmin):
    """Class to represent Coins at the admin interface"""

    list_per_page = 10
    
    search_fields = [
        'symbol',
        'name',
    ]

    list_display = [
        'symbol',
        'name',
    ]
admin.site.register(Coins, CoinsAdmin)

class CoinsOperationsAdmin(admin.ModelAdmin):
    """Class to represent Coins at the admin interface"""

    list_per_page = 10
    
    search_fields = [
        'SNo',
        'marketcap',
    ]

    list_display = [
        'SNo',
        'date',
    ]
admin.site.register(CoinsOperations, CoinsOperationsAdmin)