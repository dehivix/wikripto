import datetime
import string
import random
import pandas as pd

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views import View
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from .models import Coins, CoinsOperations
from .forms import CSVForm


@method_decorator(login_required, name='dispatch')
class CoinsListView(ListView):
    model = Coins
    template_name = 'coins/list_coins.html'
    context_object_name = 'coins'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(CoinsListView, self).get_context_data(**kwargs)
        coins = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(coins, self.paginate_by)
        try:
            coins = paginator.page(page)
        except PageNotAnInteger:
            coins = paginator.page(1)
        except EmptyPage:
            coins = paginator.page(paginator.num_pages)
        context['coins'] = coins
        return context


@method_decorator(login_required, name='dispatch')
class CoinsOperationListView(ListView):
    model = CoinsOperations
    template_name = 'coins/list_coins_operations.html'
    context_object_name = 'coins_operations'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(CoinsOperationListView, self).get_context_data(**kwargs)
        coins_operations = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(coins_operations, self.paginate_by)
        try:
            coins_operations = paginator.page(page)
        except PageNotAnInteger:
            coins = paginator.page(1)
        except EmptyPage:
            coins = paginator.page(paginator.num_pages)
        context['coins_operations'] = coins_operations
        return context


@method_decorator(login_required, name='dispatch')
class LoadCSV(View):
    form_class = CSVForm
    initial = {'key': 'value'}
    template_name = 'coins/load_data.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        tmp_data = pd.read_csv(form['csv'].data, sep=',', header=0)
        
        #Trying to get the coin if not then create.
        current_coin = Coins.objects.get_or_create(        
                name = tmp_data.Name[0], 
                symbol = tmp_data.Symbol[0],
        )

        #Saving the objects to the database.
        coins_operations_population = [
            CoinsOperations(
                coin = current_coin[0],
                SNo = tmp_data.SNo[row],
                date = tmp_data.Date[row],
                high = tmp_data.High[row],
                low = tmp_data.Low[row],
                open_val = tmp_data.Open[row],
                close_val = tmp_data.Close[row],
                volume = tmp_data.Volume[row],
                marketcap = tmp_data.Marketcap[row],

            )
            for row in range(tmp_data.SNo.count())
        ]

        try:
            CoinsOperations.objects.bulk_create(coins_operations_population, ignore_conflicts=True)
            m = 'Successfully loaded data'
            return render(request, self.template_name, {'form':form, 'success':m})

        except Exception as e:
            return render(request, self.template_name, {'form':form, 'form_errors':e})