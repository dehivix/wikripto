from django.db import models


class Coins(models.Model):
    """Model to register coins availables in wikripto."""  
    name = models.CharField(max_length=30)
    symbol = models.CharField(max_length=6)

    def __str__(self):
        """Describe the strings for coin objects.

        Returns:
            str: with sno + name.
        """
        return f'{self.name} / {self.symbol}'

    class Meta:
        verbose_name_plural = 'coins'


class CoinsOperations(models.Model):
    """Model to register coins operations availables in wikripto."""
    coin = models.ForeignKey(
        'Coins',
        on_delete=models.CASCADE,
    )
    SNo = models.IntegerField()
    date = models.DateTimeField()
    high = models.FloatField()
    low = models.FloatField()
    open_val = models.FloatField()
    close_val = models.FloatField()
    volume = models.FloatField()
    marketcap = models.FloatField()

    class Meta:
        verbose_name_plural = 'coins operations'
        unique_together = ('coin', 'SNo', 'date')
