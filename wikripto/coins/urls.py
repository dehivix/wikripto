from django.urls import path

from wikripto.coins.models import CoinsOperations
from .views import (
    CoinsListView,
    CoinsOperationListView,
    LoadCSV,
)

urlpatterns = [
    path(
        '',
        CoinsListView.as_view(),
        name='coins',
    ),
    
    path(
        'load_csv/',
        LoadCSV.as_view(),
        name='load_csv',
    ),

    path(
        'list_operations/',
        CoinsOperationListView.as_view(),
        name='list_operations',
    ),
]