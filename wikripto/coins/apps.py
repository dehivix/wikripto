from django.apps import AppConfig


class CoinsConfig(AppConfig):
    name = 'wikripto.coins'
